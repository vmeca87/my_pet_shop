from setuptools import setup


setup(name='pet-shop-api',
      version='0.1',
      author='Victor Cabello',
      author_email='vmeca87@gmail.com',
      licese='MIT',
      package=['pet_shop'],
      install_requires=[
          'docopt',
          'waitress',
          'connexion',
          'connexion[swagger-ui]'
      ],
      setup_requires=[
          "pytest-runner"
      ],
      test_require=[
          "pytest", "mock", "pytest-cov",  "coverage"
      ],
      entry_points={
          'console_scripts': [
              'pet_shop_start=pet_shop:main'
          ]
      })
