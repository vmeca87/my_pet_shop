from pet_shop.command_line import main
from unittest.mock import patch, Mock


@patch('pet_shop.command_line.serve')
@patch('pet_shop.command_line.connexion.App')
def test_main_01(app, serve):
    """
    Test default port
    """
    # Prepare
    expected_app = app.return_value = Mock()
    expected_app.configure_mock(app=None)
    # Execute
    main(argv=[])
    # Verify
    serve.assert_called_with(None, host='0.0.0.0', port='8080')


@patch('pet_shop.command_line.serve')
@patch('pet_shop.command_line.connexion.App')
def test_main_02(app, serve):
    """
    Test specific port
    """
    # Prepare
    expected_app = app.return_value = Mock()
    expected_app.configure_mock(app=None)
    # Execute
    main(argv=['--port=8181'])
    # Verify
    serve.assert_called_with(None, host='0.0.0.0', port='8181')
