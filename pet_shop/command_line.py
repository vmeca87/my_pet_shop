"""
Command Line for Pet Shop

Usage:
  pat_shop_start [--port=<p>]
  pat_shop_start -h | --help
  pat_shop_start --version

Options:
  -h --help     Show this screen.
  --version     Show version.
  --port=<p>    Port where the api runs [default: 8080].
"""
from waitress import serve
from docopt import docopt
import connexion
import sys


def main(argv=sys.argv[1:]):
    """
    Start a rest api

    This functions is called from the console script configuration defined on
    the setup.py. It main responsability is to start a produciton server
    where the REST API can runs.

    Parameters
    ----------
    argv : list
    a list of arguments passed from the terminal.
    """
    arguments = docopt(__doc__, argv=argv, version='1.0')
    app = connexion.App(__name__, specification_dir='swagger/')
    app.add_api('pet_shop.yaml')
    serve(app.app, host='0.0.0.0', port=arguments['--port'])
