"""
api_operations

This modules control the access to the PETS.

All the functions defined here are called by the REST API.
"""
from connexion import NoContent

PETS = {}


def get_pets():
    """
    Returns a dict of all the pets with their id.

    Example:
    -------

    If we have only a pet called fido
    then our dict only will contains that entry.

    >>> PETS[123]={'name': 'fido'}
    >>> get_pets()
    {123: {'name': 'fido'}}
    """
    return PETS


def get_pet(pet_id):
    """
    Returns a dict of an specfic pet based on its id.

    Example:
    --------

    1) If we use an existing id
    then we will get the data corresponding to the pet

    >>> PETS[123]={'name': 'fido'}
    >>> get_pet(123)
    {'name': 'fido'}

    2) If we use an non existing id
    then we will get a touple with message 'Not found' and
    status 404

    >>> PETS[123]={'name': 'fido'}
    >>> get_pet(3)
    ('Not found', 404)
    """
    return PETS.get(pet_id) or ('Not found', 404)


def put_pet(pet_id, pet):
    """
    Store a new pet.

    Example:
    --------

    1) if we send an id and pet informaiton
    then the PETS variable will be updated

    >>> put_pet(123, {'name': 'fido'})
    >>> PETS
    {123: {'name': 'fido'}}
    """
    PETS[pet_id] = pet


def delete_pet(pet_id):
    """
    Delete a pet

    Example:
    --------

    1) If we use an existing id
    then we will delete a pet with that id

    >>> PETS[123]={'name': 'fido'}
    >>> delete_pet(123)
    (..., 204)
    >>> PETS
    {}

    2) If we use an non existing id
    then we will got a 404 status
    status 404

    >>> PETS[123]={'name': 'fido'}
    >>> delete_pet(124)
    (..., 404)
    >>> PETS
    {123: {'name': 'fido'}}
    """
    pet = PETS.get(pet_id)
    if pet:
        del PETS[pet_id]
        return NoContent, 204
    else:
        return NoContent, 404
